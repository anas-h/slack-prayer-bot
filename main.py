#!/usr/bin/env python3

import sys
import os
import json
import csv
import requests
import inspect
from datetime import datetime


WEBHOOK = "https://hooks.slack.com/services/XXX/XXX/XXX"
CITY = "tripolilb"
CHANNEL = "#salat"
USER = "prayer-times"
CSV_URL = "http://example.com/csv_link/"
prefix = "/home/linostar/slack-slayer-bot"


def muslimsalat():
	try:
		# prefix = os.path.dirname(os.path.abspath(inspect.stack()[0][1]))
		if os.path.isfile(prefix + "/config.json"):
			with open(prefix + "/config.json", "r") as fp:
				data = json.load(fp)
				WEBHOOK = data["webhook"]
				CITY = data["city"]
				CHANNEL = data["channel"]
				USER = data["user"]
				CSV_URL = data["csv_url"]
				
		r = requests.get("http://muslimsalat.com/{}.json?method=5".format(CITY))
		if type(r.content) is bytes:
			content = r.content.decode("utf-8")
		else:
			content = r.content
		json_data = json.loads(content).get("items")
		
		if json_data:
			times = json_data[0]
			template = "Prayer times for: `{}`\nFajr: `{}`  -  Shurooq: `{}`  - Dhuhr: `{}` -  Asr: `{}`  -  Maghrib: `{}`  - Isha: `{}`"
			template_msg = template.format(times["date_for"], times["fajr"], times["shurooq"], times["dhuhr"],
				times["asr"], times["maghrib"], times["isha"])
			slack_message = {
				"channel": CHANNEL, 
				"username": USER, 
				"text": template_msg
			}

			r2 = requests.post(WEBHOOK, json=slack_message)
			# print(r2.status_code, r2.reason)
	except Exception as e:
		with open("error_logs.txt", "a") as fp:
			fp.write("{} -- {}".format(datetime.now().strftime("%Y-%m-%d %H:%M:%S"), str(e)))

def manual_csv():
	try:
		# prefix = os.path.dirname(os.path.abspath(inspect.stack()[0][1]))
		if os.path.isfile(prefix + "/config.json"):
			with open(prefix + "/config.json", "r") as fp:
				data = json.load(fp)
				WEBHOOK = data["webhook"]
				CITY = data["city"]
				CHANNEL = data["channel"]
				USER = data["user"]
				CSV_URL = data["csv_url"]

		# r = requests.get(CSV_URL)
		# for row in r.content.decode("utf-8").split("\r\n"):
		with open(prefix + "/tripoli_times.csv") as fp:
			rows = fp.readlines()[1:]
			for row in rows:
				parts = row.strip().split(",")
				if parts[0] == datetime.now().strftime("%d/%m"):
					template = "Prayer times for: `{}`\nFajr: `{}`  -  Shurooq: `{}`  - Dhuhr: `{}` -  Asr: `{}`  -  Maghrib: `{}`  - Isha: `{}`"
					template_msg = template.format(datetime.now().strftime("%Y-%m-%d"), 
						parts[1], parts[2], parts[3], parts[4], parts[5], parts[6])
					slack_message = {
						"channel": CHANNEL, 
						"username": USER, 
						"text": template_msg
					}

					r2 = requests.post(WEBHOOK, json=slack_message)
					# print(r2.status_code, r2.reason)
	except Exception as e:
		with open("error_logs.txt", "a") as fp:
			fp.write("{} -- {}".format(datetime.now().strftime("%Y-%m-%d %H:%M:%S"), str(e)))

def main():
	manual_csv()

main()
